## Overview:

This project uses puppet to install and configure Mesos masters and
slaves in 3 vagrant VMs based on a 14.04 server box. It will also install marathon and
chronos frameworks and docker to allow docker as a containerization
method. Then install some simple services on based on the above. Finally
it will install zookeeper and setup mesos/zookeeper clustering.

The environment for each task is set by using enviroment variables to
tell puppet which nodes should be running what.

Server IPs are as follows:
```
srv1 - 10.141.141.11
srv2 - 10.141.141.12
srv3 - 10.141.141.13
```
### Prerequisites:

1. Vagrant
2. This repo
3. For much speedier building install `vagrant plugin install vagrant-cachier`
4. Hosts file entry for the 3 servers:
```
10.141.141.11 srv1
10.141.141.12 srv2
10.141.141.13 srv3
```

## Task 1: 

1. First clone the repo: `git clone https://wmcg@bitbucket.org/wmcg/mesos-marathon-chronos.git` We will be working from master branch. 
2. Next start and provision the VMs: `TASK=1 vagrant up` (`TASK=1` defines
   the roles the servers take). As you can guess this will take a while.
3. Once complete access Mesos URL - [http://10.141.141.11:5050](http://10.141.141.11:5050)
4. You should be able to see registered slaves under [http://10.141.141.11:5050/#/slaves
](http://10.141.141.11:5050/#/slaves)

## Task 2: 

To run the 2 application i tried use the simplest methods.

For the echo server i just used netcat. A Dockerfile is used to build
this on each of the servers which is not ideal. Under normal
circumstances a private repo would be used.

For the hello command i just used a very simple python script to echo
the data (this was after having problems just using netcat! as an echo
client!). To get the service info i used a curl and the port was
hard coded since only host networking in docker is supported at this
time.

1. Run the provisioning with the correct enviroment var for task2. `TASK=2 vagrant provision` ` 
2. Move to the tasks-jobs folder: `cd tasks-jobs`
3. Add the echo server:
`curl -X POST -H "Content-Type: application/json" 10.141.141.11:8080/v2/apps -d@echo-docker.json | python -m json.tool`
4. Now add the chronos job:
`curl -L -H 'Content-Type: application/json' -X POST http://10.141.141.11:4400/scheduler/iso8601 -d@hello-client.json`
5. Check the status of the job via chronos and see the output from the
   sandbox for the hello chronos job in mesos is 'Hello'

## Task 3:

For this task I run master/slave/marathon on all three nodes with a
quroum of 2. 

1. Run `TASK=3 vagrant provision` and wait for it to complete.
2. At this point its necissary to reset the cluster date by running
   `./scripts/reset_cluster.sh`
3. Check that mesos redirects to the master now and that marathon leader
   proxying is working.
4. Note 3 slaves.


#### Issues encountered

* Docker support is new and only the newest version of marathon 0.7.0 supports it.
* Had many problems with mesos/marathon/zookeeper being confused by the multiple interfaces
  and often advertise the incorrect ip/hosts.

#### TODO

* Remove duplication in master/slave logic in puppet manifest with defines
* Use Augues for handling hostfile
* The run once task runs many times :)
