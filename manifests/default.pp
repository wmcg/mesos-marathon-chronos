# Puppet manifest for installing and managing mesos
include apt

# adding mesosphere key keyserver.ubuntu.com --recv E56151BF
apt::key { 'mesosphere':
  key        => 'E56151BF',
  key_server => 'keyserver.ubuntu.com'
}

# adding http://repos.mesosphere.io/ubuntu trusty-testing main
apt::source { 'mesosphere-testing':
  location    => 'http://repos.mesosphere.io/ubuntu',
  repos       => 'main',
  release     => 'trusty-testing',
  require     => Apt::Key['mesosphere'],
  include_src => false,
}

# This is needed too, because cronos is not in the testing repo
apt::source { 'mesosphere':
  location    => 'http://repos.mesosphere.io/ubuntu',
  repos       => 'main',
  release     => 'trusty',
  require     => Apt::Key['mesosphere'],
  include_src => false,
}

# Install docker repo since new version required
apt::source { 'docker':
  location    => 'https://get.docker.io/ubuntu',
  repos       => 'main',
  key         => 'A88D21E9',
  release     => 'docker',
  key_source  => 'http://get.docker.io/gpg',
  include_src => false,
}

package { 'mesos':
  ensure  => '0.20.0-0.1.201408141740.ubuntu1404',
  require => Apt::Source['mesosphere-testing'],
}
package { 'zookeeper':
  require => Exec['run-once'],
}

# docker import - root/echo < /vagrant/docker-images/echoserver.tar
package { 'lxc-docker':
  ensure  => present,
  require => [
    Apt::Source['docker'],
    Exec['run-once']
    ],
}

package { 'marathon': require => Apt::Source['mesosphere'] }

#Bit hacky. Private registry would be best way
exec { 'build docker echo image':
  command => '/usr/bin/docker build -t echo-server:v0.1 /vagrant/dockerfiles/echo/',
  require => Package['lxc-docker'],
}

exec { 'run-once':
  command => '/bin/touch /root/hostname',
  creates => '/root/hostname_update/',
  notify  => [
    Exec['add-hosts'],
    Exec['remove-localhost'],
    ],
}

# quick and dirty for now... this should ideally be done in Augeas
exec { 'add-hosts':
  command     => '/bin/echo "10.141.141.11 srv1" >> /etc/hosts && /bin/echo "10.141.141.12 srv2" >> /etc/hosts && /bin/echo "10.141.141.13 srv3" >> /etc/hosts',
  refreshonly => true,
}

exec { 'remove-localhost':
  command     => "/bin/sed -i '/127.0.1.1 ${hostname} ${hostname}/d' /etc/hosts",
  refreshonly => true,
}

# $server_type is a fact set from Vagrantfile
case $server_type {
  'master': {
    service { 'mesos-slave':
      ensure => stopped,
      require => Package['mesos'],
    }
    service { 'mesos-master':
      ensure => running,
      require => Package['mesos'],
    }
    file { '/etc/mesos-master/hostname':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-master'],
    }
    file { '/etc/mesos-master/ip':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-master'],
    }
    if $frameworks == 'true' {
      package { 'chronos':
        require => Apt::Source['mesosphere'],
      }
      service { 'chronos':
        ensure => running,
        require => Package['chronos'],
      }
      service { 'marathon':
        ensure  => running,
        require => Package['marathon'],
      }
    }
  }
  'slave': {
    service { 'mesos-slave':
      ensure  => running,
      require => Package['mesos'],
    }
    service { 'mesos-master':
      ensure  => stopped,
      require => Package['mesos'],
    }
    file { '/etc/mesos-slave/master':
      content => '10.141.141.11:5050',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    # Without this get 'duplicate flag master'
    file { '/etc/default/mesos-slave':
      content => '',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/hostname':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/ip':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/containerizers':
      content => 'docker,mesos',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/executor_registration_timeout':
      content => '2mins',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
  }
  'cluster': {
    $zk_hosts = 'zk://10.141.141.11:2181,10.141.141.12:2181,10.141.141.13:2181/mesos'
    # Bare bones zk conf
    $zk_conf = "tickTime=2000
initLimit=10
syncLimit=5
dataDir=/var/lib/zookeeper
clientPort=2181
server.1=10.141.141.11:2888:3888
server.2=10.141.141.12:2888:3888
server.3=10.141.141.13:2888:3888
"
    service { 'mesos-slave':
      ensure  => running,
      require => Package['mesos'],
    }
    service { 'mesos-master':
      ensure  => running,
      require => Package['mesos'],
    }
    service { 'zookeeper':
      ensure  => running,
      require => Package['zookeeper'],
    }
    # Without this being blank get 'duplicate flag master'
    file { '/etc/default/mesos-slave':
      content => '',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/hostname':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/ip':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/zookeeper/conf/zoo.cfg':
      content => $zk_conf,
      require => Package['zookeeper'],
      notify  => Service['zookeeper'],
    }
    file { '/etc/zookeeper/conf/myid':
      content => $server_id,
      require => Package['mesos'],
      notify  => Service['zookeeper'],
    }
    file { '/etc/mesos/zk':
      content => $zk_hosts,
      require => Package['mesos'],
      notify  => [
        Service['mesos-slave'],
        Service['mesos-master']
        ],
    }
    file { '/etc/mesos-master/quorum':
      content => '2',
      require => Package['mesos'],
      notify  => Service['mesos-master'],
    }
    file { '/etc/mesos-master/hostname':
      content => $ipaddress_eth1,
      require => Package['mesos'],
      notify  => Service['mesos-master'],
    }
    service { 'marathon':
      ensure  => running,
      require => Package['marathon'],
    }
    file { '/etc/mesos-slave/master':
      content => $zk_hosts,
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/containerizers':
      content => 'docker,mesos',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
    file { '/etc/mesos-slave/executor_registration_timeout':
      content => '2mins',
      require => Package['mesos'],
      notify  => Service['mesos-slave'],
    }
  }
  default: { warning('I dont have a type :( was TASK env var set on command line when running vagrant?') }
}

