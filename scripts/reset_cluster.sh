#!/usr/bin/env bash
# Script that will reset the state of zk/mesos/frameworks cluster

stop_services() {
  # $1 = server
  echo $(vagrant ssh $1 -c "sudo stop zookeeper; sudo stop mesos-master; sudo stop mesos-slave; sudo stop marathon; sudo stop chronos")
}

clear_data() {
  # $1 = server
  echo $(vagrant ssh $1 -c "sudo rm -rf /var/lib/mesos/replicated_log/*")
}

start_services() {
  # $1 = server
  echo $(vagrant ssh $1 -c "sudo start zookeeper; sudo start mesos-master; sudo start mesos-slave; sudo start marathon; sudo start chronos")
}

for server in srv1 srv2 srv3; do
  stop_services $server
done

for server in srv1 srv2 srv3; do
  clear_data $server
done

for server in srv1 srv2 srv3; do
  start_services $server
done

